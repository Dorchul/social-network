# Avatrade

This is a solution to Avatrade's interview task.

## Installation

Clone this repo, install conda, and then install a conda environment with all
dependencies using `conda env update -f environment.yml` from the project
directory.

Alternatively, use `python setup.py install`. You are advised to run this in a
new virtual environment.

## Usage

### Running the API server

You must include a local settings file in `socialnetwork/settings_local.py`
which must include:

- `SECRET_KEY`
- `HUNTER_API_KEY`
- `CLEARBIT_API_KEY`

Then you can run the API server using:

```sh
python manage.py runserver
```

You can optionally specify your custom settings in
`[--settings <your-settings-file>]`. The defaults are geared towards development
and not suitable for production (i.e. `DEBUG` mode is active).

### Running the test bot

Make sure to clean up the database prior to running the bot, since otherwise it
may return errors about existing usernames in the db:

```sh
rm -rf db.sqlite3 &> /dev/null
python manage.py makemigrations
python manage.py migrate
```

Then to run the bot:

```sh
python ./bot/bot.py --config-file <file> [--server-address <addr>]
```

See [bot/config_example.ini](./bot/config_example.ini) for an example config
file.

## Development

The code is formatted with yapf and linted using pylint. You need to install
pylint-django to avoid false positives from pylint on Django classes.

### Running unit tests

```sh
tox -e py37
```

### Running integration tests

From the project root:

```sh
./accounts/run_http_test.sh
```

## TODOs

- Add Dockerfile
- Consider changing URL organization
