import clearbit
from django.conf import settings
import django.db.utils
from rest_framework import generics
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from accounts import models
# pylint: disable=wildcard-import,unused-wildcard-import
from accounts.serializers import *


@api_view(['POST'])
def register_user(request):
    serializer = RegisterUserSerializer(data=request.data)
    if not serializer.is_valid():
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    user = serializer.save()
    if not settings.USE_CLEARBIT:
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    response = clearbit.Enrichment.find(email=request.data.get('email'),
                                        stream=True,
                                        key=settings.CLEARBIT_API_KEY)
    if response:
        enrichment = response['person']
        models.UserEnrichment(
            user=user,
            linkedin_handle=enrichment['linkedin']['handle'],
            twitter_handle=enrichment['twitter']['handle'],
            facebook_handle=enrichment['facebook']['handle']).save()
    return Response(serializer.data, status=status.HTTP_201_CREATED)


class PostList(generics.ListCreateAPIView):
    queryset = models.Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class LikeList(generics.ListCreateAPIView):
    queryset = models.Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def post(self, request, *args, **kwargs):
        serializer = LikeSerializer(data=request.data)
        if not serializer.is_valid():
            print(serializer.errors)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            post = models.Post.objects.get(pk=request.data['post'])
        except models.Post.DoesNotExist:
            return Response('Post does not exist',
                            status=status.HTTP_404_NOT_FOUND)
        if post.author == request.user:
            return Response('Liking your own post is not allowed',
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            return self.create(request, *args, **kwargs)
        except django.db.utils.IntegrityError:
            return Response('Double likes are not allowed',
                            status=status.HTTP_400_BAD_REQUEST)


class LikeDetail(generics.RetrieveDestroyAPIView):
    queryset = models.Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, *args, **kwargs):
        try:
            like = models.Like.objects.get(pk=kwargs['pk'])
        except models.Like.DoesNotExist:
            return Response('Like does not exist',
                            status=status.HTTP_404_NOT_FOUND)
        if like.user != request.user:
            return Response('Cannot remove likes from other users',
                            status=status.HTTP_400_BAD_REQUEST)
        return self.destroy(request, *args, **kwargs)
