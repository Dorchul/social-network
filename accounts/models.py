# pylint: disable=imported-auth-user
from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    author = models.ForeignKey(User,
                               related_name='posts',
                               on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    content = models.TextField()

    def __str__(self):
        return f'{self.author.username: self.author.content}'


class Like(models.Model):
    user = models.ForeignKey(User,
                             related_name='likes',
                             on_delete=models.CASCADE)
    post = models.ForeignKey(Post,
                             related_name='likes',
                             on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'post')


class UserEnrichment(models.Model):
    user = models.OneToOneField(User,
                                primary_key=True,
                                related_name='enriched',
                                on_delete=models.CASCADE)
    linkedin_handle = models.CharField(max_length=256, null=True)
    twitter_handle = models.CharField(max_length=256, null=True)
    facebook_handle = models.CharField(max_length=256, null=True)
